import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MyComponentComponent } from './componets/my-component/my-component.component';

@NgModule({
  declarations: [
    MyComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [MyComponentComponent]
})
export class AppModule { }
