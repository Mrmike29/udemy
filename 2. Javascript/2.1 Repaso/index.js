// alert("hello")

var nombre = "Mike";
var altura = 1.75;

var datos = document.getElementById('datos');

datos.innerHTML =
    `<h1>Caja de datos</h1>
    <h2>Me llamo ${nombre}</h2>
    <h3>Mido ${altura}</h3>
    `;

if (altura > 1.90) {
    datos.innerHTML += `<h1>Eres una persona alta</h1>`;
} else {
    datos.innerHTML += `<h1>Eres una persona baja</h1>`;
}

var coche = {
    modelo: 'Mercedez Clase A',
    velMax: '500',
    antiguedad: 2020,
    mostrarDatos(){
        console.log(this.modelo, this.velMax, this.antiguedad)
    }
}

document.write(`<h1>${coche.modelo}</h1>`)
coche.mostrarDatos()

var saludar = new Promise((resolve, reject) => {
    setTimeout(() => {
        let saludo = "Hola muy buenas a todos"

        if (saludo){
            resolve(saludo)
        }else{
            reject("No hay saludo");
        }
    }, 2000)
})

saludar.then(resultado => {
    alert(resultado)
}).catch(err => {
    alert(err);
})